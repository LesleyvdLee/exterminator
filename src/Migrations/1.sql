CREATE TABLE users_pickle (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    size INT NOT NULL,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id, user_id)
);

CREATE TABLE users_global_experience (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    xp INT NOT NULL,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_gain_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id, user_id)
);

CREATE TABLE users_server_experience (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    guild_id TEXT,
    xp INT NOT NULL,
    active BOOLEAN DEFAULT true,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_gain_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id, user_id, guild_id)
);