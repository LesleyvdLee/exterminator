DROP TABLE users_global_experience;
DROP TABLE users_server_experience;

CREATE TABLE users_message_experience (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    guild_id TEXT,
    message_count INT NOT NULL,
    random_gain_factor INT NOT NULL,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_gain_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_message TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE users_global_experience (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    xp INT NOT NULL DEFAULT 0,
    xp_towards_next INT NOT NULL DEFAULT 0,
    level INT NOT NULL DEFAULT 0,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE users_server_experience (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    user_id TEXT,
    guild_id TEXT,
    xp INT NOT NULL DEFAULT 0,
    xp_towards_next INT NOT NULL DEFAULT 0,
    level INT NOT NULL DEFAULT 0,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);