CREATE TABLE auto_roles (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    guild_id TEXT,
    role_id TEXT,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE suggestions_channels (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    guild_id TEXT,
    channel_id TEXT,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);