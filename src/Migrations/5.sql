CREATE TABLE custom_prefixes (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    guild_id TEXT,
    prefix TEXT,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);