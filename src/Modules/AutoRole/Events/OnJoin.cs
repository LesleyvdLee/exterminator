
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

using Exterminator.Modules.AutoRole.Queries;
using Exterminator.Modules.Core;

namespace Exterminator.Modules.AutoRole.Events {
    abstract class OnJoin {
        public static async Task Handle(DSharpPlus.EventArgs.GuildMemberAddEventArgs join) {
            try {
                List<AutoRoles> autoRoles = await AutoRoles.FindByGuild(join.Guild.Id.ToString());

                foreach (var role in autoRoles) {
                    DSharpPlus.Entities.DiscordRole discordRole = null;

                    try {
                        discordRole = join.Guild.Roles.First(x => x.Id.ToString() == role.roleId);
                    } catch (System.Exception) {
                        await AutoRoles.Delete(role.guildId, role.roleId);
                        continue;
                    }

                    await join.Guild.GrantRoleAsync(join.Member, discordRole);
                }
            } catch (System.Exception exception) {
                Logger.Log(exception);
            }
        }
    }
}