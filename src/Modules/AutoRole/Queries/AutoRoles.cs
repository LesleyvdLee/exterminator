using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Exterminator.Modules.Core.Database;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.AutoRole.Queries {
    public class AutoRoles {
        public Guid? id;
        public string guildId;
        public string roleId;
        public DateTime? creationDate;

        public AutoRoles(Guid? id, string guildId, string roleId,
            DateTime? creationDate) {
            this.id = id;
            this.guildId = guildId;
            this.roleId = roleId;
            this.creationDate = creationDate;
        }

        private static AutoRoles Transform(Dictionary<String, dynamic> data) {
            return new AutoRoles(data["id"], data["guild_id"], data["role_id"], data["creation_date"]);
        }

        public static async Task<AutoRoles> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM auto_roles WHERE id = @id", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("AutoRoles: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<AutoRoles>> FindByRole(string roleId, string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM auto_roles
                    WHERE role_id = @role_id AND guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, roleId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<AutoRoles> data = new List<AutoRoles>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<List<AutoRoles>> FindByGuild(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM auto_roles
                    WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<AutoRoles> data = new List<AutoRoles>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<AutoRoles> Insert(AutoRoles data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO auto_roles 
                    (guild_id, role_id) VALUES 
                    (@guild_id, @role_id) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, data.roleId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("AutoRoles: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<AutoRoles> Update(AutoRoles data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE auto_roles SET 
                    (guild_id, role_id) = 
                    (@guild_id, @role_id) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, data.roleId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("AutoRoles: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task Delete(string guildId, string roleId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                DELETE FROM auto_roles WHERE guild_id = @guild_id AND role_id = @role_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, roleId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    return;
                }
            }
        }
    }
}