using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using Exterminator.Modules.Core;
using DSharpPlus.Entities;

namespace Exterminator.Modules.Commands {
    public class Reverse : BaseCommand {
        [Command("reverse")]
        [Description("Reverses a message.")]
        public async Task Command(CommandContext ctx, DiscordMember member = null) {
            await ctx.TriggerTypingAsync();

            if (member == ctx.Client.CurrentUser) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I'm not going to reverse my own message...");
                return;
            }

            IReadOnlyList<DiscordMessage> messages;
            List<DiscordMessage> targetMessages = new List<DiscordMessage>();
            DiscordMessage targetMessage = null;

            if (member == null) {
                messages = await ctx.Channel.GetMessagesAsync(50);
            } else {
                messages = await ctx.Channel.GetMessagesAsync(100);
                foreach (DiscordMessage msg in messages) {
                    if (msg.Author.Equals(member)) {
                        targetMessages.Add(msg);
                    }
                }
            }

            if ((member != null && targetMessages.Count == 0) || messages.Count == 0) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            if (member == null) {
                int index = 1;
                while (messages[index].Content.StartsWith("!!") || messages[index].Author == ctx.Client.CurrentUser) {
                    index++;
                }
                targetMessage = messages[index];
            } else {
                int index = 0;
                while (targetMessages[index].Content.StartsWith("!!")) {
                    index++;
                }
                targetMessage = targetMessages[index];
            }

            if (targetMessage == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} no idea how, but you broke it, great....");
                return;
            }

            char[] messageArray = targetMessage.Content.ToCharArray();
            Array.Reverse(messageArray);

            await ctx.RespondAsync($"{targetMessage.Author.Mention} {(new string(messageArray))}");
        }

        public override void Register() {
            Program.CommandsNext.RegisterCommands<Reverse>();
            Logger.Log("Set up reverse command!", Logger.Level.INFO);
        }
    }
}
