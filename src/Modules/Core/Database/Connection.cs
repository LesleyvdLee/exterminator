using Npgsql;

namespace Exterminator.Modules.Core.Database {
    public static class Connection {
        private static NpgsqlConnection connection;

        public static void Connect(NpgsqlConnectionStringBuilder credentials) {
            credentials.Database = "exterminator";
            credentials.Pooling = true;
            credentials.SslMode = SslMode.Prefer;
            credentials.TrustServerCertificate = true;
            credentials.MinPoolSize = 2;
            credentials.MaxPoolSize = 20;
            connection = new NpgsqlConnection(credentials.ConnectionString);
            connection.Open();
        }

        public static NpgsqlConnection Get() {
            return connection;
        }
    }
}
