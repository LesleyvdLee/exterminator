using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.Core.Database.Queries {
    public class ExperienceRoleRewards {
        public Guid? id;
        public string guildId;
        public string roleId;
        public long level;
        public DateTime? creationDate;

        public ExperienceRoleRewards(Guid? id, string guildId, string roleId,
            long level, DateTime? creationDate) {
            this.id = id;
            this.guildId = guildId;
            this.roleId = roleId;
            this.level = level;
            this.creationDate = creationDate;
        }

        private static ExperienceRoleRewards Transform(Dictionary<String, dynamic> data) {
            return new ExperienceRoleRewards(data["id"], data["guild_id"], data["role_id"], data["level"], data["creation_date"]);
        }

        public static async Task<ExperienceRoleRewards> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM experience_role_rewards WHERE id = @id", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("ExperienceRoleRewards: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<ExperienceRoleRewards>> FindByRole(string roleId, string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM experience_role_rewards
                    WHERE role_id = @role_id AND guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, roleId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<ExperienceRoleRewards> data = new List<ExperienceRoleRewards>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<List<ExperienceRoleRewards>> FindByGuild(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM experience_role_rewards
                    WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<ExperienceRoleRewards> data = new List<ExperienceRoleRewards>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<List<ExperienceRoleRewards>> FindByGuild(long level, string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM experience_role_rewards
                    WHERE level <= @level AND guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, level);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<ExperienceRoleRewards> data = new List<ExperienceRoleRewards>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<ExperienceRoleRewards> Insert(ExperienceRoleRewards data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO experience_role_rewards 
                    (guild_id, role_id, level) VALUES 
                    (@guild_id, @role_id, @level) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, data.roleId);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("ExperienceRoleRewards: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<ExperienceRoleRewards> Update(ExperienceRoleRewards data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE experience_role_rewards SET 
                    (guild_id, role_id, level) = 
                    (@guild_id, @role_id, @level) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, data.roleId);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("ExperienceRoleRewards: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task Delete(string guildId, string roleId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                DELETE FROM experience_role_rewards WHERE guild_id = @guild_id AND role_id = @role_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, roleId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    return;
                }
            }
        }
    }
}