using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.Core.Database.Queries {
    public class UsersServerExperience {
        public Guid? id;
        public string userId;
        public string guildId;
        public long xp;
        public long xpTowardsNext;
        public long level;
        public DateTime? creationDate;

        public UsersServerExperience(Guid? id, string userId, string guildId, long xp, long xpTowardsNext,
            long level, DateTime? creationDate) {
            this.id = id;
            this.userId = userId;
            this.guildId = guildId;
            this.xp = xp;
            this.xpTowardsNext = xpTowardsNext;
            this.level = level;
            this.creationDate = creationDate;
        }

        private static UsersServerExperience Transform(Dictionary<String, dynamic> data) {
            return new UsersServerExperience(data["id"], data["user_id"], data["guild_id"], data["xp"],
                data["xp_towards_next"], data["level"], data["creation_date"]);
        }

        public static async Task<UsersServerExperience> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM users_server_experience WHERE id = @id", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersServerExperience: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<UsersServerExperience>> FindByUser(string userId, string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM users_server_experience
                    WHERE user_id = @user_id AND guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, userId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<UsersServerExperience> data = new List<UsersServerExperience>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<UsersServerExperience> Insert(UsersServerExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO users_server_experience 
                    (user_id, guild_id, xp, xp_towards_next, level) VALUES 
                    (@user_id, @guild_id, @xp, @xp_towards_next, @level) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("xp", NpgsqlDbType.Integer, data.xp);
                command.Parameters.AddWithValue("xp_towards_next", NpgsqlDbType.Integer, data.xpTowardsNext);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersServerExperience: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<UsersServerExperience> Update(UsersServerExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE users_server_experience SET 
                    (user_id, guild_id, xp, xp_towards_next, level) = 
                    (@user_id, @guild_id, @xp, @xp_towards_next, @level) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("xp", NpgsqlDbType.Integer, data.xp);
                command.Parameters.AddWithValue("xp_towards_next", NpgsqlDbType.Integer, data.xpTowardsNext);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersServerExperience: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }
    }
}