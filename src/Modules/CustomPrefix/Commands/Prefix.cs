using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using Exterminator.Modules.Core;
using Exterminator.Modules.Commands;
using Exterminator.Modules.CustomPrefix.Queries;

namespace Exterminator.Modules.CustomPrefix.Commands {

    [Group("prefix")]
    [Description("Contains subcommands for managing the bot's prefix in a guild.")]
    public class Prefix : BaseCommand {
        [Command("set")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        [Description("Sets the bot prefix for a guild.")]
        public async Task Set(CommandContext ctx, string prefix = "") {
            if (ctx.Guild == null) {
                await ctx.RespondAsync($"This command needs to be executed in a guild....");
                return;
            }

            if (prefix == "") {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a prefix to set....");
                return;
            }

            var prefixes = await CustomPrefixes.FindByGuild(ctx.Guild.Id.ToString());

            if (prefixes.Count > 0) {
                var currentPrefix = prefixes[0];
                currentPrefix.prefix = prefix;

                await CustomPrefixes.Update(currentPrefix);
            } else {
                var channel = new CustomPrefixes(null, ctx.Guild.Id.ToString(), prefix, null);
                await CustomPrefixes.Insert(channel);
            }

            await ctx.RespondAsync($"{ctx.Member.Mention} Set the guild's prefix to \"{prefix}\".");
        }

        [Command("reset")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        [Description("Resets this guild's prefix to the bot default.")]
        public async Task Remove(CommandContext ctx) {
            if (ctx.Guild == null) {
                await ctx.RespondAsync($"This command needs to be executed in a guild....");
                return;
            }

            await CustomPrefixes.Delete(ctx.Guild.Id.ToString());

            await ctx.RespondAsync($"{ctx.Member.Mention} Removed the custom prefix if it was there.");
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Prefix>();
            Logger.Log("Set up prefix module!", Logger.Level.INFO);
        }
    }
}