using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Exterminator.Modules.Core.Database;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.CustomPrefix.Queries {
    public class CustomPrefixes {
        public Guid? id;
        public string guildId;
        public string prefix;
        public DateTime? creationDate;

        public CustomPrefixes(Guid? id, string guildId, string prefix,
            DateTime? creationDate) {
            this.id = id;
            this.guildId = guildId;
            this.prefix = prefix;
            this.creationDate = creationDate;
        }

        private static CustomPrefixes Transform(Dictionary<String, dynamic> data) {
            return new CustomPrefixes(data["id"], data["guild_id"], data["prefix"], data["creation_date"]);
        }

        public static async Task<List<CustomPrefixes>> FindByGuild(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM custom_prefixes
                    WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<CustomPrefixes> data = new List<CustomPrefixes>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<CustomPrefixes> Insert(CustomPrefixes data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO custom_prefixes 
                    (guild_id, prefix) VALUES 
                    (@guild_id, @prefix) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("prefix", NpgsqlDbType.Text, data.prefix);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("CustomPrefixes: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<CustomPrefixes> Update(CustomPrefixes data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE custom_prefixes SET 
                    (guild_id, prefix) = 
                    (@guild_id, @prefix) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("prefix", NpgsqlDbType.Text, data.prefix);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("CustomPrefixes: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task Delete(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                DELETE FROM custom_prefixes WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    return;
                }
            }
        }
    }
}