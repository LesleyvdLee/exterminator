using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Commands;
using Exterminator.Modules.Core;

using Exterminator.Modules.Suggestions.Queries;

namespace Exterminator.Modules.Suggestions.Commands {
    public class Suggest : BaseCommand {
        /* 
         * Implements the Command() function which is called when the
         * CommandsNext Module triggers it
         */
        [Command("suggest")]
        public async Task Command(CommandContext ctx, [RemainingText] string suggestion = "") {
            if (ctx.Guild == null) {
                await ctx.RespondAsync($"{ctx.User.Mention} You need to suggest in a guild, not DMs");
                return;
            }

            if (suggestion == "") {
                await ctx.RespondAsync($"{ctx.User.Mention} You can't send in an empty suggestion");
                return;
            }

            List<SuggestionsChannels> channels = await SuggestionsChannels.FindByGuild(ctx.Guild.Id.ToString());

            DiscordChannel channel = null;

            if (channels.Count > 0) {
                try {
                    channel = ctx.Guild.Channels.First(x => x.Id.ToString() == channels[0].channelId);
                } catch (System.Exception) {
                    await SuggestionsChannels.Delete(ctx.Guild.Id.ToString());
                }
            }

            if (channel == null) {
                await ctx.RespondAsync($"{ctx.User.Mention} This guild doesn't have suggestions set up");
                return;
            }

            var embed = new DiscordEmbedBuilder();
            embed = embed.WithAuthor("Suggestion", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");
            embed = embed.WithThumbnailUrl(ctx.User.AvatarUrl);
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.AddField("Submitter", ctx.User.Mention);
            embed = embed.AddField("Suggestion", suggestion);

            var message = await channel.SendMessageAsync(null, false, embed);
            await message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, ":thumbsup:"));
            await message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, ":thumbsdown:"));
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<Suggest>();
            Logger.Log("Set up suggest command!", Logger.Level.INFO);
        }
    }
}