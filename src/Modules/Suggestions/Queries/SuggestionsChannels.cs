using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Exterminator.Modules.Core.Database;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.Suggestions.Queries {
    public class SuggestionsChannels {
        public Guid? id;
        public string guildId;
        public string channelId;
        public DateTime? creationDate;

        public SuggestionsChannels(Guid? id, string guildId, string channelId,
            DateTime? creationDate) {
            this.id = id;
            this.guildId = guildId;
            this.channelId = channelId;
            this.creationDate = creationDate;
        }

        private static SuggestionsChannels Transform(Dictionary<String, dynamic> data) {
            return new SuggestionsChannels(data["id"], data["guild_id"], data["channel_id"], data["creation_date"]);
        }

        public static async Task<SuggestionsChannels> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM auto_roles WHERE id = @id", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("SuggestionsChannels: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<SuggestionsChannels>> FindByRole(string roleId, string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM suggestions_channels
                    WHERE role_id = @role_id AND guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("role_id", NpgsqlDbType.Text, roleId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<SuggestionsChannels> data = new List<SuggestionsChannels>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<List<SuggestionsChannels>> FindByGuild(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM suggestions_channels
                    WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<SuggestionsChannels> data = new List<SuggestionsChannels>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<SuggestionsChannels> Insert(SuggestionsChannels data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO suggestions_channels 
                    (guild_id, channel_id) VALUES 
                    (@guild_id, @channel_id) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("channel_id", NpgsqlDbType.Text, data.channelId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("SuggestionsChannels: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<SuggestionsChannels> Update(SuggestionsChannels data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE suggestions_channels SET 
                    (guild_id, channel_id) = 
                    (@guild_id, @channel_id) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("channel_id", NpgsqlDbType.Text, data.channelId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("SuggestionsChannels: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task Delete(string guildId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                DELETE FROM suggestions_channels WHERE guild_id = @guild_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, guildId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    return;
                }
            }
        }
    }
}