using System.Threading.Tasks;
using System.Collections.Generic;

using Exterminator.Modules.Core.Database.Queries;
using Exterminator.Modules.Core;

using DSharpPlus.Entities;

namespace Exterminator.Modules.Xp {
    abstract class Global {
        static public async Task AddXp(DSharpPlus.EventArgs.MessageCreateEventArgs msg, long xp) {
            try {
                List<UsersGlobalExperience> messages = await UsersGlobalExperience.FindByUser(msg.Author.Id.ToString());

                long oldLevel = 0;
                long newLevel = 0;
                long originalXpGain = xp;
                long levelXp = GetLevelXp(newLevel);

                if (messages.Count == 0) {
                    while (levelXp < xp) {
                        xp -= levelXp;
                        newLevel += 1;
                        levelXp = GetLevelXp(newLevel);
                    }

                    UsersGlobalExperience data = new UsersGlobalExperience(null, msg.Author.Id.ToString(),
                        originalXpGain, xp, newLevel, null, null);

                    await UsersGlobalExperience.Insert(data);
                } else {
                    UsersGlobalExperience message = messages[0];

                    if (message == null) {
                        while (levelXp < xp) {
                            xp -= levelXp;
                            newLevel += 1;
                            levelXp = GetLevelXp(newLevel);
                        }

                        UsersGlobalExperience data = new UsersGlobalExperience(null, msg.Author.Id.ToString(),
                            originalXpGain, xp, newLevel, null, null);

                        await UsersGlobalExperience.Insert(data);
                    } else {
                        UsersGlobalExperience data = message;

                        oldLevel = data.level;
                        newLevel = data.level;
                        levelXp = GetLevelXp(oldLevel);
                        data.xp += originalXpGain;
                        data.xpTowardsNext += originalXpGain;

                        while (levelXp < data.xpTowardsNext) {
                            data.xpTowardsNext -= levelXp;
                            newLevel += 1;
                            levelXp = GetLevelXp(newLevel);
                        }

                        data.level = newLevel;

                        await UsersGlobalExperience.Update(data);
                    }
                }

                if (oldLevel != newLevel) {
                    DiscordEmbedBuilder embed = new DiscordEmbedBuilder();
                    embed.WithAuthor("Congratulations!", null, msg.Client.CurrentUser.AvatarUrl);
                    embed = embed.WithColor(DiscordColor.Orange);
                    embed.WithThumbnailUrl(msg.Author.AvatarUrl);
                    embed.WithDescription($"{msg.Author.Mention} congratulations! You advanced to level {newLevel}!");
                    await msg.Channel.SendMessageAsync(null, false, embed);
                }
            } catch (System.Exception exception) {
                Logger.Log(exception);
            }
        }

        static public long GetLevelXp(long level) {
            return (250 * level) + 250;
        }
    }
}