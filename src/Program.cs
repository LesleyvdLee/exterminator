﻿using System;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;

using Exterminator.Modules.Core;
using Exterminator.Modules.Core.Database;
using Exterminator.Modules.Xp;
using Npgsql;

namespace Exterminator {
    class Program {
        public static DiscordClient Discord;
        public static CommandsNextModule CommandsNext;
        public static InteractivityModule Interactivity;

        public static void Main(string[] args) {
            if (Config.get.discordToken != "") {
                var config = new DiscordConfiguration {
                    Token = Config.get.discordToken,
                    TokenType = TokenType.Bot,
                    AutoReconnect = true
                };
                Run(config).ConfigureAwait(false).GetAwaiter().GetResult();
            } else {
                Logger.Log("Please set your Discord Token in the config.json file", Logger.Level.EXEP);
            }
        }

        /* 
         * Sets up the PostgreSQL Database connection string        
         */
        private static async Task DatabaseConnection() {
            NpgsqlConnectionStringBuilder postgresql = new NpgsqlConnectionStringBuilder();
            if (Config.get.postgresql.user != "" || Config.get.postgresql.password != "") {
                postgresql.Username = Config.get.postgresql.user;
                postgresql.Password = Config.get.postgresql.password;
                postgresql.Port = Config.get.postgresql.port;
                postgresql.Host = Config.get.postgresql.ip;

                Connection.Connect(postgresql);
                await Migrations.Run();
            } else {
                Logger.Log("Please set your PostgreSQL credentials in the config.json file", Logger.Level.INFO);
                Environment.Exit(1);
            }
        }

        private static async Task Run(DiscordConfiguration config) {
            await DatabaseConnection();

            Discord = new DiscordClient(config);
            await Discord.ConnectAsync();

            Interactivity = Discord.UseInteractivity(new InteractivityConfiguration());

            // !! TODO: temporarily disable no anime ban
            // Discord.MessageCreated += new AsyncEventHandler<DSharpPlus.EventArgs.MessageCreateEventArgs>(NoAnime.OnMessage);

            // Message hook for the XP system
            Discord.MessageCreated += new AsyncEventHandler<DSharpPlus.EventArgs.MessageCreateEventArgs>(Generic.OnMessage);

            // AutoRole hook
            Discord.GuildMemberAdded += new AsyncEventHandler<DSharpPlus.EventArgs.GuildMemberAddEventArgs>(Modules.AutoRole.Events.OnJoin.Handle);

            RichPresence.SetClient(Discord);
            Discord.Heartbeated += new AsyncEventHandler<DSharpPlus.EventArgs.HeartbeatEventArgs>(RichPresence.update);

            CommandsNext = Discord.UseCommandsNext(new CommandsNextConfiguration {
                CustomPrefixPredicate = Exterminator.Modules.CustomPrefix.Predicate.CustomPrefixPredicate
            });

            CommandsNext.SetHelpFormatter<Exterminator.HelpFormatter>();

            CommandsNext.CommandErrored
                += new AsyncEventHandler<DSharpPlus.CommandsNext.CommandErrorEventArgs>(Exterminator.Modules.Commands.BaseCommand.onError);

            /* 
             * Registers all the commands using the ICommand interface
             */
            Commands.Load();

            await Task.Delay(-1);
        }
    }
}
